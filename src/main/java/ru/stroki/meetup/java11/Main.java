package ru.stroki.meetup.java11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
  private static final String INTRO =
      "Данная програма запущена под управлением платформы JDK "
          + System.getProperty("java.version");
  private static final String LARGE_STRING =
      "Line 1 Line 1 Line 1 Line 1 Line 1 Line 1\n"
          + "Line 2 Line 2 Line 2 Line 2 Line 2\n"
          + "Line 3 Line 3 Line 3 Line 3";

  public static void main(String[] args) throws IOException {
    System.out.println("\n### --- String --- ###\n");
    strings();
    System.out.println("\n### --- Optional --- ###\n");
    optionals();
    System.out.println("\n### --- Switch --- ###\n");
    switchMoscowToPerm();
    System.out.println("\n### --- Record --- ###\n");
    record();
    System.out.println("\n### --- NullPointerException --- ###\n");
    npe();
    System.out.println("\n### --- Stream --- ###\n");
    stream();
  }

  private static void strings() {
    if (INTRO != null && !INTRO.isBlank()) { // Проверяем, что строка состоит не только из пробелов
      System.out.println(INTRO);
    }
    System.out.println(LARGE_STRING);
    System.out.println("Lines count: " + LARGE_STRING.lines().count());
  }

  private static void optionals() {
    Set<Optional<String>> elements =
        Set.of(Optional.of("It's Java 11!"), Optional.ofNullable(null));
    elements.forEach(
        e ->
            e.ifPresentOrElse(
                System.out::println, () -> System.out.println("Optional string is not present!")));
  }

  private static void switchMoscowToPerm() throws IOException {
    System.out.println(
        "Как вы будете добираться до Перми из Москвы?\n1. На поезде\n2. На самолёте");
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    Integer type = Integer.parseInt(bufferedReader.readLine());
    WayToTravel myType =
        type == 1 ? WayToTravel.TRAIN : type == 2 ? WayToTravel.AIRBUS : WayToTravel.UNDEFINED;
    String result;
    switch (myType) {
      case TRAIN:
        result = "Время в пути 19 часов 56 минут!";
        break;
      case AIRBUS:
        result = "Время в пути 2 часа 5 минут!";
        break;
      default:
        result = "Это как?";
    }
    System.out.println(result);
  }

  private enum WayToTravel {
    TRAIN,
    AIRBUS,
    UNDEFINED
  }

  private static void record() {
    AnyDto example1 = new AnyDto(1, "yes", true);
    AnyDto example2 = new AnyDto(2, "no", false);
    System.out.println(example1);
    System.out.println(example2);
    System.out.println(example1.equals(example2));
  }

  private static void npe() {
    Map<String, String> map = new HashMap<>();
    System.out.println(map.get("key").toLowerCase());
  }

  private static void stream() {
    List<Integer> numbers = List.of(1, 2, 6, 7, 9, 11, 14, 18, 19);
    List<Integer> filtered = numbers.stream().filter(n -> n % 2 == 0).collect(Collectors.toList());
    System.out.println(filtered);
  }
}
